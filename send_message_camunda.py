import uuid

from camunda.client.engine_client import EngineClient


def main():
    client = EngineClient()
    resp_json = client.correlate_message(message_name="esbTechCheck",
                                         # business_key="b9a59098-2ba3-11ed-83ac-201e88222490",
                                         process_variables={"intVar2": 3, "strVar2": "helloWorld", 'json2': {'as': 21}},
                                         process_instance_id='2b69fe2a-2ba8-11ed-8802-201e8822248c')
    return resp_json


if __name__ == '__main__':
    main()

