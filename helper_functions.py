import json
import os
from pathlib import Path


def create_folder(folder_name):
    folder_full_path = f"{os.path.dirname(__file__)}/{folder_name}"
    Path(folder_full_path).mkdir(parents=True, exist_ok=True)


def write_json_file_to_folder(file_name, folder_name, data_write):
    create_folder(folder_name)
    folder_full_path = f"{os.path.dirname(__file__)}/{folder_name}"
    file_full_name = os.path.join(folder_full_path, f"{file_name}.json")
    with open(file_full_name, 'w') as outfile:
        json.dump(data_write, outfile, indent=2, sort_keys=True)


def write_text_file_to_folder(file_name, folder_name, data_write):
    create_folder(folder_name)
    folder_full_path = f"{os.path.dirname(__file__)}/{folder_name}"
    file_full_name = os.path.join(folder_full_path, f"{file_name}.txt")
    with open(file_full_name, 'w') as outfile:
        outfile.write(str(data_write))


def read_json_file_from_folder(file_name, folder_name):
    folder_full_path = f"{os.path.dirname(__file__)}/{folder_name}"
    file_full_name = os.path.join(folder_full_path, f"{file_name}.json")
    with open(file_full_name) as read_file:
        file_json_data = json.load(read_file)
    return file_json_data


if __name__ == '__main__':
    pass
