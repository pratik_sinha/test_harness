from fastapi import FastAPI
from helper_functions import read_json_file_from_folder, write_json_file_to_folder
from enum import Enum
from typing import Optional, Union
from pydantic import BaseModel

app = FastAPI()


class Direction(Enum):
    north = 'North'
    south = 'South'
    east = 'East'
    west = 'West'


class BookData(BaseModel):
    book_title: str
    book_author: str


class BookDataInstance(BaseModel):
    book_data: BookData


@app.get("/")
async def read_all_books(skip_book: Optional[str] = None):
    if skip_book:
        book_data_copy = read_json_file_from_folder("books", "json_files/books_data")
        if skip_book in book_data_copy.keys():
            del book_data_copy[skip_book]
        return book_data_copy
    return read_json_file_from_folder("books", "json_files/books_data")


@app.get("/books/{book_title}")
async def read_book(book_title: str):
    data = read_json_file_from_folder("books", "json_files/books_data")
    return {'book_title': book_title}


@app.get("/{book_title}")
async def read_book_1(book_title):
    data = read_json_file_from_folder("books", "json_files/books_data")
    return {'book_data': data.get(book_title)}


@app.post("/")
async def create_book_entry(book_data: BookData):
    data = read_json_file_from_folder("books", "json_files/books_data")
    book_index_list = [int(x.strip('_')[-1]) for x in data.keys()]
    book_index_list.sort()
    book_id_to_create = f'book_{book_index_list[-1] + 1}'
    data[book_id_to_create] = {
        "title": book_data.book_title,
        "author": book_data.book_author
    }
    try:
        write_json_file_to_folder('books', 'json_files/books_data', data)
    except Exception as e:
        return {'error': f'error in writing to the file with error: {str(e)}'}
    return data[book_id_to_create]


@app.put("/{book_id}")
async def create_book_entry(book_id: str, book_data: BookData):
    data = read_json_file_from_folder("books", "json_files/books_data")
    if book_id in data.keys():
        data[book_id] = {
            "title": book_data.book_title,
            "author": book_data.book_author
        }
        try:
            write_json_file_to_folder('books', 'json_files/books_data', data)
        except Exception as e:
            return {'error': f'error in writing to the file with error: {str(e)}'}
        return data[book_id]
    else:
        return {'error': f'error in updating the book id provided'}


@app.delete("/{book_id}")
async def delete_book_entry(book_id: str):
    data = read_json_file_from_folder("books", "json_files/books_data")
    if book_id in data.keys():
        deleted_data = data.get(book_id, None)
        del data[book_id]
        try:
            write_json_file_to_folder('books', 'json_files/books_data', data)
        except Exception as e:
            return {'error': f'error in deleting data from file: {str(e)}'}
        return {'deleted_data': deleted_data}
    else:
        return {'error': f'error in finding the book id provided'}


@app.get("/direction/{direction}")
async def get_direction(direction: Direction):
    if direction == 'East':
        return {'direction': direction.east, 'arrow': 'right'}
    elif direction == 'West':
        return {'direction': direction.west, 'arrow': 'left'}
    elif direction == direction.north:
        return {'direction': direction.north, 'arrow': 'up'}
    elif direction == 'South':
        return {'direction': direction.south, 'arrow': 'down'}