import uuid

from camunda.client.engine_client import EngineClient


def main():
    client = EngineClient()
    resp_json = client.start_process(process_key="PROVIDE_SUB_JORNEY",
                                     tenant_id="67e68a01-2113-437d-951b-90b7b5b72db2",
                                     variables={"intVar": "1", "strVar": "hello", 'jsonData': {'a': 1213}},
                                     business_key=str(uuid.uuid1()))
    return resp_json


if __name__ == '__main__':
    main()